var gulp = require('gulp');
var sass = require('gulp-sass');
var sourcemaps = require('gulp-sourcemaps');
var concat = require('gulp-concat');
var uglify = require('gulp-uglify');
var tinypng = require('gulp-tinypng-compress');
var htmlmin = require('gulp-htmlmin');
var deleteLines = require('gulp-delete-lines');
var realFavicon = require('gulp-real-favicon');
var fs = require('fs');

gulp.task('sass', function () {
    return gulp.src('./src/sass/**/*.scss')
        .pipe(sourcemaps.init())
        .pipe(sass({
            // nested, expanded, compact, compressed
            outputStyle: 'compressed'
        }).on('error', sass.logError))
        .pipe(sourcemaps.write('./'))
        .pipe(gulp.dest('./dist/css'));
});

gulp.task('js', function () {
    return gulp.src('./src/js/**/*.js')
        .pipe(sourcemaps.init())
        .pipe(concat('all.js'))
        .pipe(uglify())
        .pipe(sourcemaps.write('./'))
        .pipe(gulp.dest('./dist/js'));
});

gulp.task('tinypng', function () {
    return gulp.src('./src/img/**/*.{png,jpg,jpeg}')
        .pipe(tinypng({
            key: '6_2Zvi5VbHQSrJKPfShhwUz5CjDdD97l',
            sigFile: './src/img/.tinypng-sigs',
            log: true
        }))
        .pipe(gulp.dest('./dist/img'));
});

gulp.task('htmlmin', function () {
    return gulp.src(['./src/**/*.html'])
        .pipe(deleteLines({
            'filters': [
                /<link href="\.\.\/dist\/css\/styles.css" rel="stylesheet">/g
            ]
        }))
        .pipe(deleteLines({
            'filters': [
                /<script src="\.\.\/dist\/js\/all\.js"><\/script>/g
            ]
        }))
        .pipe(htmlmin({
            collapseWhitespace: true,
            minifyCSS: true,
            minifyJS: true,
            removeComments: true
        }))
        .pipe(gulp.dest('./dist'));
});

gulp.task('templateshtmlmin', function () {
    return gulp.src('./src/**/*.tpl.html')
        .pipe(htmlmin({
            collapseWhitespace: true,
            minifyCSS: true,
            minifyJS: true,
            removeComments: true
        }))
        .pipe(gulp.dest('./dist'));
});

gulp.task('copy-files', function () {
    var copy = {
        files: ['./src/.htaccess', './src/**/*.xml', './src/**/*.ico', './src/**/*.json', './src/**/*.svg']
    };
    return gulp.src(copy.files, {
            base: "./src/"
        })
        .pipe(gulp.dest('./dist'));
});

/**
 * Favicon
 */

var FAVICON_DATA_FILE = 'faviconData.json';
var APP_NAME = 'gps-do-roweru.sklep.pl';
var THEME_COLOR = '#338fad';
var START_URL = 'http://www.gps-do-roweru.sklep.pl/';

// Generate the icons.
gulp.task('generate-favicon', function (done) {
    realFavicon.generateFavicon({
        masterPicture: 'src/img/icons/master-favicon.png',
        dest: 'src/img/icons',
        iconsPath: 'img/icons',
        design: {
            ios: {
                pictureAspect: 'noChange',
                assets: {
                    ios6AndPriorIcons: false,
                    ios7AndLaterIcons: false,
                    precomposedIcons: false,
                    declareOnlyDefaultIcon: true
                },
                appName: APP_NAME
            },
            desktopBrowser: {},
            windows: {
                pictureAspect: 'noChange',
                backgroundColor: THEME_COLOR,
                onConflict: 'override',
                assets: {
                    windows80Ie10Tile: false,
                    windows10Ie11EdgeTiles: {
                        small: false,
                        medium: true,
                        big: false,
                        rectangle: false
                    }
                },
                appName: APP_NAME
            },
            androidChrome: {
                pictureAspect: 'noChange',
                themeColor: THEME_COLOR,
                manifest: {
                    name: APP_NAME,
                    startUrl: START_URL,
                    display: 'standalone',
                    orientation: 'notSet',
                    onConflict: 'override',
                    declared: true
                },
                assets: {
                    legacyIcon: false,
                    lowResolutionIcons: false
                }
            },
            safariPinnedTab: {
                pictureAspect: 'silhouette',
                themeColor: THEME_COLOR
            }
        },
        settings: {
            scalingAlgorithm: 'Mitchell',
            errorOnImageTooSmall: false
        },
        markupFile: FAVICON_DATA_FILE
    }, function () {
        done();
    });
});

// Inject the favicon markups in your HTML pages.
gulp.task('inject-favicon-markups', function () {
    return gulp.src(['src/**/*.html', '!src/googlecf68af412099da49.html'])
        .pipe(realFavicon.injectFaviconMarkups(JSON.parse(fs.readFileSync(FAVICON_DATA_FILE)).favicon.html_code))
        .pipe(gulp.dest('src'));
});

// Check for updates on RealFaviconGenerator
gulp.task('check-for-favicon-update', function () {
    var currentVersion = JSON.parse(fs.readFileSync(FAVICON_DATA_FILE)).version;
    realFavicon.checkForUpdates(currentVersion, function (err) {
        if (err) {
            throw err;
        }
    });
});

/**
 * Build
 */
gulp.task('build', ['sass', 'js', 'tinypng', 'htmlmin', 'templateshtmlmin', 'copy-files'], function () {
    gulp.watch('./src/sass/**/*.scss', ['sass']);
    gulp.watch('./src/js/**/*.js', ['js']);
    gulp.watch('./src/**/*.html', ['htmlmin', 'templateshtmlmin']);
});